freefem++ (4.15+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update hardening flags.
  * Ignore unsupported LD/CPP flags from hdf5 library (Closes: #1095401)
  * Create manual page for md2edp

 -- Francois Mazen <mzf@debian.org>  Sun, 16 Feb 2025 21:22:48 +0100

freefem++ (4.14+dfsg-1) unstable; urgency=medium

  * New upstream version.

 -- Francois Mazen <mzf@debian.org>  Sat, 13 Apr 2024 17:40:52 +0200

freefem++ (4.13+dfsg-1) unstable; urgency=medium

  [Debian Janitor]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libscalapack-mpi-dev.

  [Francois Mazen]
  * New upstream version 4.13
  * Remove obsolete patches
  * Add patch to remove obsolete binary_function and unary_function,
    and fix failing autopkgtest (Closes: #1033702)
  * Remove allow-stderr restriction for build-and-run-lib autopkgtest,
    because not necessary anymore due to above patch
  * Bump standard version to 4.6.2

 -- Francois Mazen <francois@mzf.fr>  Sat, 17 Jun 2023 16:45:34 +0200

freefem++ (4.11+dfsg1-3) unstable; urgency=high

  * Add allow-stderr restriction for build-and-run-lib (Closes: #1033702)

 -- Francois Mazen <francois@mzf.fr>  Sat, 08 Apr 2023 15:19:26 +0200

freefem++ (4.11+dfsg1-2) unstable; urgency=medium

  * Limit autopkgtest architectures to allow testing migration (Closes:
    #992924)

 -- Francois Mazen <francois@mzf.fr>  Sun, 17 Apr 2022 14:09:49 +0200

freefem++ (4.11+dfsg1-1) unstable; urgency=medium

  * New upstream version
  * Update debian/copyright for automatic repack
  * Bump standard version to 4.6.0.1

 -- Francois Mazen <francois@mzf.fr>  Sat, 09 Apr 2022 21:11:07 +0200

freefem++ (4.9+dfsg1-2) unstable; urgency=medium

  * Fix blhc false positive with ff-c++
  * Fix invalid uploader (Closes: #992381)
  * Configure with --enable-generic to fix baseline violation
    (Closes: #924009)

 -- Francois Mazen <francois@mzf.fr>  Tue, 17 Aug 2021 22:45:22 +0200

freefem++ (4.9+dfsg1-1) unstable; urgency=medium

  * New upstream version (Closes: #969022)
  * Bump standard version to 4.5.1
  * Set debhelper-compat to 13
  * Set Rules-Requires-Root to no
  * Remove trailing whitespaces
  * Set secure url in copyright file
  * Remove obsolete patches
  * Fix native encoding of some files
  * Fix missing hardening flags
  * Add man page for ffmaster
  * Fix file path in ffmaster
  * Add upstream metadata
  * Add missing dependency (unzip)
  * Fix config.h PACKAGE_NAME collision by using AX_PREFIX_CONFIG_H
  * Add auto package tests
  * Add debian/clean file
  * Add Francois Mazen as uploader

 -- Francois Mazen <francois@mzf.fr>  Fri, 21 May 2021 22:23:38 +0200

freefem++ (3.61.1+dfsg1-6) unstable; urgency=high

  * Team upload.
  * Fix FTBFS with GCC-10 (Closes: #957233)

 -- Francois Mazen <francois@mzf.fr>  Thu, 13 May 2021 14:06:33 +0200

freefem++ (3.61.1+dfsg1-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with gsl 2.6 (Closes: #960010)

 -- Stefano Rivera <stefanor@debian.org>  Sun, 17 May 2020 13:41:04 -0700

freefem++ (3.61.1+dfsg1-5) unstable; urgency=medium

  * Team upload.
  * Build-Depends non-versioned libpetsc-real-dev and libpetsc-complex-dev
    Closes: #939663
  * debhelper-compat 12
  * Rework manual changes in source tree to quilt patches

 -- Andreas Tille <tille@debian.org>  Sat, 05 Oct 2019 09:21:24 +0200

freefem++ (3.61.1+dfsg1-4) unstable; urgency=medium

  * Enforce BD to libpetsc-(real,complex)3.10-dev (Closes: #917977)
  * Update standards version

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sun, 06 Jan 2019 23:28:01 +0200

freefem++ (3.61.1+dfsg1-3) unstable; urgency=medium

  * Fix compilation with gcc-8/g++-8 (Closes: #897751 #915732)
  * Update standards version to 4.2.1.5

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sat, 08 Dec 2018 11:04:55 +0200

freefem++ (3.61.1+dfsg1-2) unstable; urgency=medium

  * Add gcc-7 and g++-7 to Build-Depends in d/control

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 09 Aug 2018 11:13:45 +0300

freefem++ (3.61.1+dfsg1-1) unstable; urgency=medium

  * Update d/changelog
  * New upstream version 3.61.1
  * Update debhelper to >= 11~
  * Update d/compat to 11
  * Update debian-policy to 4.1.5
  * Update Vcs-Browser and Vcs-Git fields in d/control to salsa.d.o
  * Build with gcc_7/g++-7
  * Remove --parallel and --with-autoreconf options from dh in d/rules
  * Add libtet1.5-dev to Build-Depends in d/control
  * Remone autotools-dev and dh_autoreconf from B-D in d/control
  * Remove debian/folder /freefem++-doc file
  * Add libpetsc-real3.9-dev and libpetsc-complex3.9-dev to B-D
  * Refresh examples-bamg.patch and examples++-load patch

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Wed, 08 Aug 2018 03:28:16 +0300

freefem++ (3.47+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * Build-Depends: libscalapack-mpi-dev (>= 2)
    - remove Build-Depends: libblacs-mpi-dev
      (BLACS is now included in scalapack2)
  * Standards-Version: 4.1.0
  * debhelper compatibility level 10
  * Activate SCOTCH support

 -- Drew Parsons <dparsons@debian.org>  Mon, 11 Sep 2017 01:26:04 +0800

freefem++ (3.51+dfsg1-1) UNRELEASED; urgency=medium

  * New upstream version 3.51+dfsg1

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Wed, 25 Jan 2017 15:47:03 +0200

freefem++ (3.47+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 3.47+dfsg1
  * Disable package testsuite execution at build time (Closes: #823449 #824256)

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 16 Jun 2016 10:49:59 +0300

freefem++ (3.46+dfsg1-2) unstable; urgency=medium

  * Correct build_date_from_SOURCE_DATE_EPOCH.patch
  * Update standards version
  * Increase output from testsuite checks, to hopefully avert FTBFS on several
    arches

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Tue, 03 May 2016 23:34:07 +0300

freefem++ (3.46+dfsg1-1) unstable; urgency=medium

  * Prepare for dfsg source version by excluding Apple files
  * Imported Upstream version 3.46+dfsg1
  * Rearrange dversionmangle in d/watch file
  * Set ls locale in Makefiles for reproducible order (Closes: #820815)
  * Get build date from SOURCE_DATE_EPOCH to make the build reproducible
  * Fix architecture name for hurd-i386
  * Enable testsuite at build time

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sun, 24 Apr 2016 15:01:02 +0300

freefem++ (3.45-1) unstable; urgency=medium

  * Imported Upstream version 3.45
  * Remove Files-Excluded line from d/copyright

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sat, 19 Mar 2016 22:32:31 +0200

freefem++ (3.44-1) unstable; urgency=medium

  * Imported Upstream version 3.44
  * Run cme fix dpkg and refresh patches
  * Bump standards version

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sat, 27 Feb 2016 16:25:43 +0200

freefem++ (3.38.1-1) unstable; urgency=medium

  * Imported Upstream version 3.38.1 (Closes: #793237)
  * Add libhdf5-dev to build-deps (Closes: #797117)

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sun, 30 Aug 2015 22:47:43 +0300

freefem++ (3.37.1-1) unstable; urgency=medium

  * Imported Upstream version 3.37.1
  * Clean up d/rules, d/source/options and .gitignore

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Mon, 01 Jun 2015 10:48:53 +0300

freefem++ (3.37-1) unstable; urgency=medium

  * Imported Upstream version 3.37
  * Update examples++-load.patch to avoid FTBFS

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sat, 16 May 2015 21:43:28 +0300

freefem++ (3.36.1-1) experimental; urgency=medium

  * Imported Upstream version 3.36.1
  * Remove d/copyright section for src/Algo directory

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Mon, 13 Apr 2015 21:51:49 +0300

freefem++ (3.36-1) experimental; urgency=medium

  * Exclude the Apple ._* files from the upstream tarball
  * Imported Upstream version 3.36
  * Fix d/copyright according to DEP5

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Fri, 27 Mar 2015 18:48:07 +0200

freefem++ (3.35-1) experimental; urgency=medium

  * Imported Upstream version 3.35
  * Refresh examples++-load.patch

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Fri, 13 Mar 2015 15:13:52 +0200

freefem++ (3.32.1-1) experimental; urgency=medium

  * Edit d/watch file to fix broken downloaded upstream tarball
  * Imported Upstream version 3.32.1
  * Update standards version

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Tue, 30 Dec 2014 16:21:29 +0200

freefem++ (3.31-2-1) unstable; urgency=low

  * Imported Upstream version 3.31-2

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Mon, 21 Jul 2014 14:15:13 +0300

freefem++ (3.30-1) unstable; urgency=medium

  * Imported Upstream version 3.30

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Fri, 25 Apr 2014 13:47:29 +0300

freefem++ (3.27-1) unstable; urgency=medium

  * Imported Upstream version 3.27

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Mon, 03 Mar 2014 13:31:46 +0200

freefem++ (3.26-2-3) unstable; urgency=low

  * Change package urgency from medium to low

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 26 Dec 2013 19:30:00 +0200

freefem++ (3.26-2-2) unstable; urgency=medium

  [ Dimitrios Eftaxiopoulos ]
  * Enable build with metis

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 26 Dec 2013 16:13:46 +0200

freefem++ (3.26-2-1) unstable; urgency=low

  * Imported Upstream version 3.26-2 (Closes: #706714)
  * Disable make check execution in d/rules (Closes: #730739)
  * Correct architecture names for hurd and kfreebsd (Closes: #730738)
  * Make libmesh.a install only in libfreefem++ binary
  * Fix duplication of executables (Closes: #701161)
  * Update standards version
  * Set installation dir of WHERE* scripts to /usr/include/freefem++
  * Add rdfind and symlinks to build deps in d/control

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 19 Dec 2013 22:38:49 +0200

freefem++ (3.25-1) unstable; urgency=low

  [ Dimitrios Eftaxiopoulos ]
  * Imported Upstream version 3.25 (Closes: #701161 #706714)
  * Change installation directory of header-like *.idp files
    from /usr/lib/freefem++ to /usr/include/freefem++, in order
    to fix a lintian warning
  * Update patch to examples++-load/Makefile.am in order to enable
    functioning of load *.so and include *.idp commands in *.edp
    scripts
  * Delete patches to src/Graphics/sansgraph.cpp and
    src/Graphics/xglrgraph.cpp because they are not needed any more
  * Fix lintian warning about missing LDFLAGS
  * Override dh_auto_test in debian/rules, such that in case it is
    used, it completes executing all *.edp example files, regardless
    of aborting on some of them
  * Add libmetis-dev to build-deps in d/control
  * Remove libparmetis-dev from build deps
  * Add --parallel option to dh $@ in debian/rules
  * Add hardening compilation flags to mpic++
  * Allow testing of compiling and running the example files after build

  [ Christophe Trophime ]
  * update C. Trophime email
  * add support for nlopt, ipopt - simplify debian/rules
  * upload CT changes to 3.20
  * add patch for configure
  * add patch for examples++-mpi
  * fix bamg install
  * add corrected scripts to build plugins
  * add patch for properly build examples++-load
  * add lintian overrides for libfreefem++
  * add some missing files
  * update patches
  * update rules
  * reorder BuildDepends - comment out unsupported libs

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 12 Sep 2013 00:02:58 +0300

freefem++ (3.20-1) experimental; urgency=low

  * New upstream release.
  * Add libnlopt-dev and coinor-libipopt-dev to build-deps.
  * Update patch examples++-load-Makefile.patch.
  * Remove patch examples-load-WHERE-LIBRARY.patch.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 18 Oct 2012 14:14:12 +0300

freefem++ (3.19.1-1) unstable; urgency=low

  * New upstream release.
  * Make debian/compat=9.
  * Make debhelper (>= 9~) in Build-Depends in debian/control.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sun, 03 Jun 2012 22:16:18 +0300

freefem++ (3.19-1) unstable; urgency=low

  * New upstream release.
  * Add libmumps-seq-dev to build-deps.
  * Update standards version to 3.9.3.
  * Fix FTBFS due to new g++-4.7 (Closes: #672612).

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sun, 12 May 2012 03:59:00 +0200

freefem++ (3.18.1-1) unstable; urgency=low

  * New upstream release.
  * Create new patch for documentation build.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Fri, 17 Feb 2012 21:29:00 +0200

freefem++ (3.18-1) unstable; urgency=low

  * New upstream release.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sat, 4 Feb 2012 21:10:00 +0200

freefem++ (3.17-2) unstable; urgency=low

  * Fix build failure on hurd-i386 architecture.
  * Rename kfreebsd.patch to examples++-load-load-link-in.patch and ammend it.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Thu, 29 Dec 2011 16:31:00 +0200

freefem++ (3.17-1) unstable; urgency=low

  * New upstream release.
  * Add build-deps for libgmm++-dev.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sun, 27 Nov 2011 14:17:00 +0200

freefem++ (3.14-1) unstable; urgency=low

  * Add build-deps for mumps_ptscotch, mumps_scotch and hypre.
  * Change build-dep libfltk1.1-dev to libfltk1.3-dev | libfltk-dev in debian/
    control file.
  * New upstream release.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Wed, 14 Sep 2011 19:11:00 +0300

freefem++ (3.13.3-1) unstable; urgency=low

  * Add patch for the location of file freefem++.pref.
  * New upstream release.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Tue, 9 Aug 2011 15:31:00 +0300

freefem++ (3.13.2-1) unreleased; urgency=low

  * Revert to gcc-4.6 for building.
  * New upstream release.

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Sun, 3 Jul 2011 21:51:00 +0300

freefem++ (3.13-2) unstable; urgency=low

  * Change build-dep from openmpi-bin to mpi-default-dev to effect build on
    more arches.
  * Apply patch kfreebsd.patch to fix build failure on kfreebsd arches.
  * Rename /usr/bin/bamg to /usr/bin/ffbamg to fix double occurence of the
    former, in both rheolef and freefem++ packages (Closes: #630864).

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Mon, 20 Jun 2011 23:00:00 +0200

freefem++ (3.13-1) unstable; urgency=low

  * Initial release (Closes: #500755).
  * Built with gcc-4.5 (see bug report 49264 submitted to the gcc bugzilla
    upstream).

 -- Dimitrios Eftaxiopoulos <eftaxi12@otenet.gr>  Mon, 13 Jun 2011 11:10:00 +0200
